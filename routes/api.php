<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('/rooms', 'RoomController@getAll');

Route::get('/rooms/{name}', 'RoomController@getRoomByName');

Route::get('/rooms/{roomId}/topics', 'TopicController@getTopicsByRoom');

Route::get('/topics/getTopicsByRoomName/{RoomName}', 'TopicController@getTopicsByRoomName');

// Get values of array topics
Route::post('/values/topicsLastValue', 'ValueController@getTopicsLastValue');
