<?php

use Illuminate\Database\Seeder;

class RoomsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \Illuminate\Support\Facades\DB::table('rooms')->insert([
            'key' => 'flower',
            'name' => 'Flower Room',
            'created_at' => \Carbon\Carbon::now()
        ]);

        \Illuminate\Support\Facades\DB::table('rooms')->insert([
            'key' => 'curing',
            'name' => 'Curing Room',
            'created_at' => \Carbon\Carbon::now()
        ]);

        \Illuminate\Support\Facades\DB::table('rooms')->insert([
            'key' => 'nursery',
            'name' => 'Nursery Room',
            'created_at' => \Carbon\Carbon::now()
        ]);

        \Illuminate\Support\Facades\DB::table('rooms')->insert([
            'key' => 'equipment',
            'name' => 'Equipment Room',
            'created_at' => \Carbon\Carbon::now()
        ]);
    }
}
