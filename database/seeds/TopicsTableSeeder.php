<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TopicsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /*
         * Flower Room Topics
         */
        DB::table('topics')->insert([
            'room_id' => 1, // Flower Room
            'name' => 'Temperature',
            'key' => 'temperature',
            'topic' => 'F/T',
            'description' => 'Temperature',
            'value_type' => 'number',
            'unit' => 'C°',
            'is_sensor_tab' => true,
            'created_at' => \Carbon\Carbon::now()
        ]);

        DB::table('topics')->insert([
            'room_id' => 1, // Flower Room
            'name' => 'Humidity',
            'key' => 'humidity',
            'topic' => 'F/H_S',
            'description' => 'Humidity',
            'value_type' => 'number',
            'unit' => '%',
            'is_sensor_tab' => true,
            'created_at' => \Carbon\Carbon::now()
        ]);

        DB::table('topics')->insert([
            'room_id' => 1, // Flower Room
            'name' => 'CO2',
            'key' => 'co2',
            'topic' => 'F/C',
            'description' => 'CO2',
            'value_type' => 'number',
            'unit' => '%',
            'is_sensor_tab' => true,
            'created_at' => \Carbon\Carbon::now()
        ]);

        DB::table('topics')->insert([
            'room_id' => 1, // Flower Room
            'name' => 'Light Current Status',
            'key' => 'lightStatus',
            'topic' => 'F/Ls_S',
            'description' => 'Light Current Status',
            'value_type' => 'boolean',
            'unit' => null,
            'is_sensor_tab' => false,
            'created_at' => \Carbon\Carbon::now()
        ]);
    }
}
