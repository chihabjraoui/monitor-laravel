<?php

use Illuminate\Database\Seeder;

class ValuesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \Illuminate\Support\Facades\DB::table('values')->insert([
            'topic_id' => 1, // Topic Temperature
            'value' => '23',
            'created_at' => \Carbon\Carbon::now()
        ]);

        \Illuminate\Support\Facades\DB::table('values')->insert([
            'topic_id' => 2, // Topic Humidity
            'value' => '50',
            'created_at' => \Carbon\Carbon::now()
        ]);

        \Illuminate\Support\Facades\DB::table('values')->insert([
            'topic_id' => 3, // Topic CO2
            'value' => '30',
            'created_at' => \Carbon\Carbon::now()
        ]);
    }
}
