<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTopicsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('topics', function (Blueprint $table)
        {
            $table->increments('id');
            $table->integer('room_id')->index()->unsigned();

            $table->string('name');
            $table->string('key');
            $table->string('topic');
            $table->string('description');
            $table->string('value_type');
            $table->string('unit', 10)->nullable();
            $table->boolean('is_sensor_tab')->default(false);
            $table->timestamps();

            $table->foreign('room_id')->references('id')->on('rooms')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('topics');
    }
}
