<?php

namespace App\Enumerations;

abstract class Rooms
{
    const Flower = 'flower';
    const Nursing = 'nursing';
    const Curing = 'curing';
    const Equipment = 'equipment';
}
