<?php

namespace App\Http\Controllers;

use App\Value;
use Illuminate\Http\Request;

class ValueController extends Controller
{
    public function getTopicsLastValue(Request $request)
    {
        $topicsIds = $request->get('topicsIds');

        return Value::whereIn('topic_id', $topicsIds)
            ->orderBy('created_at', 'desc')
            ->get();
    }
}
