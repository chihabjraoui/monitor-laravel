<?php

namespace App\Http\Controllers;

use App\Room;
use App\Topic;
use Illuminate\Http\Request;

class TopicController extends Controller
{
    public function getTopicsByRoom($roomId)
    {
        return Topic::where('room_id', $roomId)->get();
    }

    public function getTopicsByRoomName($roomName)
    {
        return Room::firstWhere('key', $roomName)->topics;
    }
}
