<?php

namespace App\Http\Controllers;

use App\Room;
use Illuminate\Http\Request;

class RoomController extends Controller
{
    public function getAll()
    {
        return Room::all();
    }

    public function getRoomByName($name)
    {
        return Room::firstWhere('key', $name);
    }
}
