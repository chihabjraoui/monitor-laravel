<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Topic extends Model
{

    protected $casts = [
        'is_sensor_tab' => 'boolean'
    ];

    public function values()
    {
        return $this->hasMany('App\Value');
    }

    public function lastValue()
    {
        return $this->values()/*->orderBy('created_at', 'desc')->take(1)*/;
    }
}
